pipeline {
    agent any

    environment {
        String nugetHome = "nuget"
        String xbuildHome = "xbuild"
        String xunitHome = "./packages/xunit.runner.console.2.2.0/tools/"
        String solution = "FociSolutions.KeyStoreManager.sln"
        String fetchUrl = "${env.global_keyStoreFetchUrl}"
        String pushUrl = "${env.global_keyStorePushUrl}"
    }

    options {
        timeout(time: 20, unit: 'MINUTES')
        gitlabBuilds(builds: ["jenkinsfile-clean", "checkout", "build", "unit-test", "update-artifacts", "deploy-public", "post-build"])
    }

    stages {
        stage('jenkinsfile-clean'){
            steps {
                deleteDir()
            }
            post {
                success {
                    echo 'posting success to GitLab'
                    updateGitlabCommitStatus(name: 'jenkinsfile-clean', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                    updateGitlabCommitStatus(name: 'jenkinsfile-clean', state: 'failed')
                }
            }
        }

        stage('checkout'){
            steps {
                checkout(
                    [$class: 'GitSCM', branches: [[name: 'master']], 
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [], 
                    submoduleCfg: [],
                    userRemoteConfigs: [[credentialsId: 'Jenkins_deploy_private_key', url: 'git@gitlab.com:foci-dotnet/Foci.DotNetKeyStore.git']]])
            }
            post {
                success {
                    echo 'posting success to GitLab'
                    updateGitlabCommitStatus(name: 'checkout', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                    updateGitlabCommitStatus(name: 'checkout', state: 'failed')
                }
            }
        }

        stage('build'){
            steps {
                // Restore packages and build.
                sh nugetHome + ' restore ' + solution
                sh xbuildHome + ' ' + solution
            }
            post {
                success {
                    echo 'posting success to GitLab'
                    updateGitlabCommitStatus(name: 'build', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                    updateGitlabCommitStatus(name: 'build', state: 'failed')
                }
            }
        }

        stage('unit-test'){
            steps {
                sh 'mono ' + xunitHome + 'xunit.console.exe \
                ./FociSolutions.KeyStoreManager.Tests/bin/Debug/FociSolutions.KeyStoreManager.Tests.dll \
                -nunit root.test.report -parallel all -noshadow'
            }
            post {
                success {
                    echo 'posting success to GitLab'
                    updateGitlabCommitStatus(name: 'unit-test', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                    updateGitlabCommitStatus(name: 'unit-test', state: 'failed')
                }
            }
        }

        stage('update-artifacts'){
            steps {
                sh 'rm -rf /home/jenkins/Foci.DotNetKeyStore'
                sh 'mkdir -p /home/jenkins/Foci.DotNetKeyStore'
                sh 'cp -r ./FociSolutions.KeyStore/FociSolutions.KeyStore/bin/Debug/* /home/jenkins/Foci.DotNetKeyStore'
            }
            post {
                success {
                    echo 'posting success to GitLab'
                    updateGitlabCommitStatus(name: 'update-artifacts', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                    updateGitlabCommitStatus(name: 'update-artifacts', state: 'failed')
                }
            }
        }

        stage('deploy-public'){
            steps {
                // Publish to public repo
                sshagent(['Jenkins_deploy_private_key']) {
                    sh 'git clone --mirror ' + fetchUrl
                    dir('./Foci.DotNetKeyStore.git') {
                        sh 'git remote set-url --push origin ' + pushUrl
                        sh 'git fetch -p origin'
                        sh 'git push --mirror'
                    }
                }
            }
            post {
                success {
                    echo 'posting success to GitLab'
                    updateGitlabCommitStatus(name: 'deploy-public', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                    updateGitlabCommitStatus(name: 'deploy-public', state: 'failed')
                }
            }
        }
    }

    post {
        always {
            script {
                [$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: '"${env.global_recipients}"', sendToIndividuals: true]
                echo 'posting success to GitLab'
                updateGitlabCommitStatus(name: 'post-build', state: 'success')
            }
        }
    }
}
