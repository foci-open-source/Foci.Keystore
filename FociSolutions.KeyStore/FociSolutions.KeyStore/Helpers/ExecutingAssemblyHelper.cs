﻿using System;
using System.Reflection;

namespace FociSolutions.KeyStore.Helpers
{
    public static class ExecutingAssemblyHelper
    {
        /// <summary>
        /// Gets the directory of the executing assembly
        /// </summary>
        /// <returns>String representation of the directory path.</returns>
        public static string GetExecutingAssemblyLocation()
        {
            var fileUri = new Uri(Assembly.GetExecutingAssembly().Location);
            var directory = new Uri(fileUri, ".");
            return directory.LocalPath;
        }

    }
}
