﻿using FociSolutions.KeyStore.FileAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

namespace FociSolutions.KeyStore.FileAccess
{
    /// <summary>
    /// File accessor. Used only as a wrapper to the File static class for unit testing.
    /// Excluded because of the thin wrapper.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class FileAccessor : IFileAccessor
    {

        /// <summary>
        /// Create  a new file given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to create.</param>
        public void CreateFile(string filePath)
        {
            File.Create(filePath);
        }

        /// <summary>
        /// Read a file from the file system for a given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to be read.</param>
        /// <returns>string of the file content</returns>
        public string ReadFile(string filePath)
        {
            return File.ReadAllText(filePath);
        }

        /// <summary>
        /// Write to a file  a given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to delete.</param>
        public void WriteFile(string filePath, string fileContent)
        {
            File.WriteAllText(filePath, fileContent);
        }

        /// <summary>
        /// Append to a file  a given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to delete.</param>
        public void AppendFile(string filePath, string fileContent)
        {
            File.AppendAllText(filePath, fileContent);
        }

        /// <summary>
        /// Deletes a file from the file system for a given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to delete.</param>
        /// <returns>true if file exists</returns>
        public bool FileExists(string filePath)
        {
            return File.Exists(filePath);
        }

        /// <summary>
        /// Deletes a file from the file system for a given file path.
        /// </summary>
        public void DeleteFile(string filePath)
        {
            File.Delete(filePath);
        }

    }
}
