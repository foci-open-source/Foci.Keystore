﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FociSolutions.KeyStore.FileAccess.Interfaces
{
    /// <summary>
    /// Internal Interface to create, access, modify, or delete a file from the file system.
    /// </summary>
    internal interface IFileAccessor
    {
        /// <summary>
        /// Create  a new file given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to create.</param>
        void CreateFile(string filePath);

        /// <summary>
        /// Read a file from the file system for a given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to be read.</param>
        string ReadFile(string filePath);

        /// <summary>
        /// Write to a file  a given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to delete.</param>
        void WriteFile(string filePath, string fileContent);

        /// <summary>
        /// Append to a file  a given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to delete.</param>
        void AppendFile(string filePath, string fileContent);


        /// <summary>
        /// Deletes a file from the file system for a given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to delete.</param>
        bool FileExists(string filePath);


        /// <summary>
        /// Deletes a file from the file system for a given file path.
        /// </summary>
        /// <param name="filePath"> The path of the file to delete.</param>
        void DeleteFile(string filePath);

    }

}
