﻿using FociSolutions.KeyStore.FileAccess;
using FociSolutions.KeyStore.Models;
using FociSolutions.KeyStore.Repositories;
using FociSolutions.KeyStore.Services;


namespace FociSolutions.KeyStore
{
    public static class Keystore
    {
        #region private
        private static readonly KeyCollectionService keystoreCollection;
        private static readonly KeystoreEncryptionService encryptionService;
        private static readonly  FileRepository fileRepository;
        private static readonly FileAccessor fileAccessor;
        #endregion

        /// <summary>
        /// Static Constructor 
        /// </summary>
        static Keystore()
        {
            fileAccessor = new FileAccessor();
            fileRepository = new FileRepository(fileAccessor);
            encryptionService = new KeystoreEncryptionService();
            keystoreCollection = new KeyCollectionService(encryptionService, fileRepository);
        }

        #region public members
        /// <summary>
        /// Initialize the keystore 
        /// </summary>
        /// <returns>true if the Initialization is successful</returns>
        public static bool Initialize()
        {
            return keystoreCollection.InitializeKeyCollection();
        }


        /// <summary>
        /// Insert an entry into the keystore collection
        /// </summary>
        /// <returns>returns true if successful</returns>
        public static bool Insert<T>(T keystoreEntry) where T : IKeystoreEntry
        {

            return keystoreCollection.Insert(keystoreEntry);
        }


        /// <summary>
        /// Get a keystore entry using  a unique alias
        /// </summary>
        /// <param name="alias">The unique identifier of a keystore entry.</param>
        /// <returns>a single keystore collection entry</returns>
        public static T Get<T>(string alias) where T : IKeystoreEntry
        {
            return keystoreCollection.Get<T>(alias);
        }

        /// <summary>
        /// Remove a keystore entry by alias
        /// </summary>
        /// <param name="alias">The unique identifier of a keystore entry.</param>
        /// <returns>true if deletion process is successul</returns>
        public static bool Remove<T>(string alias) where T : IKeystoreEntry
        {
            return keystoreCollection.Remove<T>(alias);
        }


        /// <summary>
        /// Update an exisiting keystore entry
        /// </summary>
        /// <param name="alias">The unique identifier of a keystore entry.</param>
        /// <param name="keystoreEntry">The keystore entry.</param>
        /// <returns>true if update process is successul</returns>
        public static bool Update<T>(string alias, T keystoreEntry) where T : IKeystoreEntry
        {
            return keystoreCollection.Update<T>(alias, keystoreEntry);
        }

        /// <summary>
        /// Insert a keystore entry of type certificate
        /// </summary>       
        /// <param name="certificateFileLocation">The  location of the certifcate file on disk.</param>
        /// <param name="certificateToken">The  certificate token.</param>
        /// <returns>true if insertion of the certificate is successul</returns>

        public static bool InsertCertificate<T>(
            string certificateFileLocation,
            T certificateToken) where T : IKeystoreEntry
        {
            return keystoreCollection.InsertCertificate<T>(certificateFileLocation, certificateToken);
        }

        #endregion
    }
}
