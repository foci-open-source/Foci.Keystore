namespace FociSolutions.KeyStore.Repositories.Interface
{
    /// <summary>
    /// Interface for a file repository. 
    /// </summary>
    internal interface IFileRepository
    {
        /// <summary>
        /// Create a file in specified location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <returns>Flag indicating the success or failure of the operation.</returns>
        bool Create(string fileLocation);

        /// <summary>
        /// Writes string data to file in a specific location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <param name="keyCollectionContent">New collection to be written in a file.</param>
        /// <returns>Flag indicating the success or failure of the operation.</returns>
        bool Write(string fileLocation, string keyCollectionContent);

        /// <summary>
        /// Read contents of a file in specified location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <returns>Returns string of the file content.</returns>
        string Read(string fileLocation);

        /// <summary>
        /// Appends content to a file in specified location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <param name="keyCollectionContent">Collection content to be written in a file.</param>
        /// <returns>Flag indicating the success or failure of the operation.</returns>
        bool Append(string fileLocation, string keyCollectionContent);

        /// <summary>
        /// Deletes a file in specified location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <returns>Flag indicating the success or failure of the operation.</returns>
        bool Delete(string fileLocation);
    }
}
