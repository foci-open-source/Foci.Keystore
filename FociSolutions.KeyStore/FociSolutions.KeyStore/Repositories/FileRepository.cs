﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FociSolutions.KeyStore.Models;
using NLog;
using FociSolutions.KeyStore.Repositories.Interface;
using System.IO;
using System.Threading;
using FociSolutions.KeyStore.FileAccess.Interfaces;
using FociSolutions.KeyStore.FileAccess;
using System.Runtime.CompilerServices;

namespace FociSolutions.KeyStore.Repositories
{
    /// <summary>
    /// Internal file repository to create, access, modify, and delete a keystore file.
    /// </summary>
    internal class FileRepository : IFileRepository
    {
        #region Private Members
        private ReaderWriterLockSlim fileLock = new ReaderWriterLockSlim();
        static private ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly IFileAccessor fileAccessor;

        #endregion
        #region IFileRepository Members 

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="fileAccessor">File accessor to perform operations in a file</param>
        public FileRepository(IFileAccessor fileAccessor)
        {
            this.fileAccessor = fileAccessor;
        }

        /// <summary>
        /// Create a file in specified location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <returns>Flag indicating the success or failure of the operation.</returns>
        public bool Create(string fileLocation)
        {

            if (fileAccessor.FileExists(fileLocation))
            {
                logger.Warn(string.Format("File already exists"));
                return true;
            }

            try
            {
                fileAccessor.CreateFile(fileLocation);
            }
            catch (IOException e)
            {
                logger.Error("Source: {0}; Message {1}; Type {2}.", e.Source, e.Message, e.GetType().ToString());
                logger.Trace("IO Exception Stack Trace: {0}", e.StackTrace);
                logger.Error(e);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Read contents of a file in specified location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <returns>Returns string of the file content.</returns>
        public string Read(string fileLocation)
        {
            var fileContent = string.Empty;

            if (!fileAccessor.FileExists(fileLocation))
            {
                logger.Warn(string.Format("{0} does not exist", fileLocation));
                throw new ArgumentException(string.Format("{0} does not exist", fileLocation), "fileLocation");
            }

            fileLock.EnterReadLock();

            try
            {
                fileContent = fileAccessor.ReadFile(fileLocation);

            }

            catch (IOException e)
            {
                logger.Error("Source: {0}; Message {1}; Type {2}.", e.Source, e.Message, e.GetType().ToString());
                logger.Trace("IO Exception Stack Trace: {0}", e.StackTrace);
                logger.Error(e);
            }

            finally
            {
                fileLock.ExitReadLock();
            }
            return fileContent;
        }

        /// <summary>
        /// Writes string data to file in a specific location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <param name="keyCollectionContent">New collection to be written in a file.</param>
        /// <returns>Flag indicating the success or failure of the operation.</returns>
        public bool Write(string fileLocation, string fileContent)
        {
            fileLock.EnterWriteLock();
            try
            {
                fileAccessor.WriteFile(fileLocation, fileContent);
            }
            catch (IOException e)
            {
                logger.Error("Source: {0}; Message {1}; Type {2}.", e.Source, e.Message, e.GetType().ToString());
                logger.Trace("IO Exception Stack Trace: {0}", e.StackTrace);
                logger.Error(e);
                return false;
            }

            finally
            {
                fileLock.ExitWriteLock();
            }

            return true;
         }

        /// <summary>
        /// Appends content to a file in specified location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <param name="keyCollectionContent">Collection content to be written in a file.</param>
        /// <returns>Flag indicating the success or failure of the operation.</returns>
        public bool Append(string fileLocation, string keyCollectionContent)
        {
            fileLock.EnterWriteLock();
            try
            {
                fileAccessor.AppendFile(fileLocation, keyCollectionContent);
            }
            catch (IOException e)
            {
                logger.Error("Source: {0}; Message {1}; Type {2}.", e.Source, e.Message, e.GetType().ToString());
                logger.Trace("IO Exception Stack Trace: {0}", e.StackTrace);
                logger.Error(e);
                return false;
            }

            finally
            {
                fileLock.ExitWriteLock();
            }

            return true;
        }

        /// <summary>
        /// Deletes a file in specified location.
        /// </summary>
        /// <param name="fileLocation">Path where the file is located.</param>
        /// <returns>Flag indicating the success or failure of the operation.</returns>
        public bool Delete(string fileLocation)
        {
            fileLock.EnterWriteLock();

            if (!fileAccessor.FileExists(fileLocation))
            {
                logger.Warn(string.Format("{0} does not exist", fileLocation));
                throw new ArgumentException(string.Format("{0} does not exist", fileLocation), "fileLocation");
            }
            try
            {
                fileAccessor.DeleteFile(fileLocation);
            }
            catch (IOException e)
            {
                logger.Error("Source: {0}; Message {1}; Type {2}.", e.Source, e.Message, e.GetType().ToString());
                logger.Trace("IO Exception Stack Trace: {0}", e.StackTrace);
                logger.Error(e);
                return false;
            }

            finally
            {
                fileLock.ExitWriteLock();
            }

            return true;
        }

        #endregion
    }
}
