﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Describes the type of keystore credential in a keystore entry.
    /// </summary>
    public enum TokenKeyType
    {
        KEYPAIR = 0,
        TOKEN,
        CERTIFICATE,
        USERNAMETOKEN
    }

}
