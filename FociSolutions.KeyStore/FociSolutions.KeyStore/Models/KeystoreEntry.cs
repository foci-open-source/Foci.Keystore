﻿
namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Generic type of credential from keystore.
    /// </summary>
    public class KeystoreEntry : IKeystoreEntry
    {
        /// <summary>
        /// Alias related to the credentials saved in the keystore
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// Type of keystore entry associated with the credentials and alias
        /// </summary>
        public TokenKeyType Type { get; set; }
        /// <summary>
        /// Additional data that describes the credentials, like credentials' expiry date and time
        /// </summary>
        public MetaData MetaData { get; set; }
    }
}
