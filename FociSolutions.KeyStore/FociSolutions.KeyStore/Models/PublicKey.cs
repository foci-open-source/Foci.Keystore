﻿
namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Public key from a keypair or standalone key.
    /// </summary>
    public class PublicKey : Key
    {
    }
}
