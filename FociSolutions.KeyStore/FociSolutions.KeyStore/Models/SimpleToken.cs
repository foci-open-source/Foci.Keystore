﻿
namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Type of credential from keystore generic simple token 
    /// </summary>
    public class SimpleToken : KeystoreEntry
    {
        /// <summary>
        /// generic simple token content
        /// </summary>
        public string Token { get; set; }
    }

}
