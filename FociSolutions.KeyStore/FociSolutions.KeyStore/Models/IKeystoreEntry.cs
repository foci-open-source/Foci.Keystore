﻿
namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Public interface for common Keystore entry properties.
    /// </summary>
    public interface IKeystoreEntry
    {
        string Alias { get; set; }
        TokenKeyType Type { get; set; }
        MetaData MetaData { get; set; }

    }
}
