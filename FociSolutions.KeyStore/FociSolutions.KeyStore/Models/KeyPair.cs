﻿using FociSolutions.KeyStore.Models;


namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Type of credential from keystore for key pairs. 
    /// </summary>
    public class KeyPair : KeystoreEntry
    { 
        /// <summary>
        /// Property for a public key
        /// </summary>
        public PublicKey PublicKey { get; set; }
        /// <summary>
        /// Property for a private key
        /// </summary>
        public PrivateKey PrivateKey { get; set; }

        /// <summary>
        /// Flag to indicate if the key pair is part of a certificate
        /// </summary>
        public bool IsCert { get; set; }
    }
}
