﻿
namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Type of credential from keystore for username/password credential
    /// </summary>
    public class UsernameToken : KeystoreEntry
    {
        /// <summary>
        /// Default constructor where the type is specified as USERNAMETOKEN.
        /// </summary>
        public UsernameToken()
        {
            this.Type = TokenKeyType.USERNAMETOKEN;
        }

        /// <summary>
        /// Attribute for username returned by the keystore.
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Attribute for password returned by the keystore.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Attribute for token returned by the keystore.
        /// </summary>
        public string Token { get; set; }
    }
}
