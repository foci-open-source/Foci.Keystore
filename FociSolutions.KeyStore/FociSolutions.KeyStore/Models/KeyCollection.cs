﻿using System.Collections.Generic;

namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Class containing a collection for keystore entries
    /// </summary>
    public class KeyCollection
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public KeyCollection()
        {

            this.KeyStoreEntries = new List<IKeystoreEntry>();
        }

        /// <summary>
        /// Collection for keystore entries.
        /// </summary>
        public IList<IKeystoreEntry> KeyStoreEntries { get; set; }
    }
}
