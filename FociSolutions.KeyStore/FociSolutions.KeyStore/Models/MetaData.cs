﻿using System;

namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Addtional information that describes credentials.
    /// </summary>
    public class MetaData
    {
        /// <summary>
        /// Expiry date of a credential
        /// </summary>
        public DateTime ExpiryTime { get; set; }
    }
}
