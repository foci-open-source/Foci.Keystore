﻿namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Model for Private or Public Key
    /// </summary>
    public class Key
    {
        /// <summary>
        /// Property for the content in a key
        /// </summary>
        public string KeyContent { get; set; }
    }
}
