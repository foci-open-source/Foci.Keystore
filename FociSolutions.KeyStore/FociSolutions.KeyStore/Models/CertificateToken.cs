﻿
namespace FociSolutions.KeyStore.Models
{
    /// <summary>
    /// Type of credential from keystore for certificates. 
    /// </summary>
    public class CertificateToken : KeystoreEntry
    {
        /// <summary>
        /// Contains raw data returned by the keystore.
        /// </summary>
        public byte[] RawData { get; set; }
    }
}
