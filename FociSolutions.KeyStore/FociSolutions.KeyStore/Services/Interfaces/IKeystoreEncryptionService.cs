﻿using FociSolutions.KeyStore.Models;
using System.Runtime.CompilerServices;


namespace FociSolutions.KeyStore.Services.Interface
{
    /// <summary>
    /// Interface for Encryption Service. Encryption and decryption of a key collection.
    /// </summary>
    internal interface IKeystoreEncryptionService
    {
        /// <summary>
        /// Decrypts the data provided and returns a key collection.
        /// </summary>
        /// <param name="KeyStoreEncryptedData">Encrypted data to be rendered to a KeyCollection</param>
        /// <returns>Key collection from the keystore.</returns>
        KeyCollection DecryptKeyStore(string KeyStoreEncryptedData);
        /// <summary>
        /// Encrypts a key collection and returns the encrypted result.
        /// </summary>
        /// <param name="KeyStore">The key collection to encrypt.</param>
        /// <returns>encrypted key collection as a string</returns>
        string EncryptKeyStore(KeyCollection KeyStore);

    }
}
