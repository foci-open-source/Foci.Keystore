﻿using FociSolutions.KeyStore.Models;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("FociSolutions.KeyStoreManager.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace FociSolutions.KeyStore.Services.Interface
{
    /// <summary>
    /// Internal interface for keycollection service
    /// </summary>
    internal interface IKeyCollectionService
    {
        /// <summary>
        /// Initializes a key collection.
        /// </summary>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        bool InitializeKeyCollection();
        /// <summary>
        /// Inserts a keystore entry into the key collection.
        /// </summary>
        /// <typeparam name="T">a type of keystore entry</typeparam>
        /// <param name="keystoreEntry">Keystore entry with credentials for the key collection.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        bool Insert<T>(T keystoreEntry) where T : IKeystoreEntry;
        /// <summary>
        /// Inserts a certificate keystore entry into the key collection.
        /// </summary>
        /// <typeparam name="T">A type of certificate keystore entry.</typeparam>
        /// <param name="certificateFileLocation">Path where certificate is located.</param>
        /// <param name="keystoreEntry">Keystore entry with credentials for the key collection.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        bool InsertCertificate<T>(string certificateFileLocation, T keystoreEntry) where T : IKeystoreEntry;
        /// <summary>
        /// Retrieves the credentials found in the keystore with the provided alias.
        /// </summary>
        /// <typeparam name="T">A type of keystore entry.</typeparam>
        /// <param name="alias">Alias needed to retrieve credentials associated to it from keystore.</param>
        /// <returns>keystore entry populated with proper credentials retrieved from keystore.</returns>
        T Get<T>(string alias) where T : IKeystoreEntry;
        /// <summary>
        /// Removes from keystore, the credentials associated with the alias provided.
        /// </summary>
        /// <typeparam name="T">A type of keystore entry.</typeparam>
        /// <param name="alias">Alias needed to retrieve credentials associated to it from keystore.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        bool Remove<T>(string alias) where T : IKeystoreEntry;
        /// <summary>
        /// Updates the credentials associated with the provided alias with the keystore entry provided.
        /// </summary>
        /// <typeparam name="T">A type of keystore entry.</typeparam>
        /// <param name="alias">Alias needed to retrieve credentials associated to it from keystore.</param>
        /// <param name="keystoreEntry">Keystore entry with credentials for the key collection.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        bool Update<T>(string alias, T keystoreEntry) where T : IKeystoreEntry;

    }
}