﻿using EnsureThat;
using FociSolutions.KeyStore.Cache;
using FociSolutions.KeyStore.Configs;
using FociSolutions.KeyStore.Helpers;
using FociSolutions.KeyStore.Models;
using FociSolutions.KeyStore.Repositories.Interface;
using FociSolutions.KeyStore.Services.Interface;
using NLog;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace FociSolutions.KeyStore.Services
{
    /// <summary>
    /// Service to manage the key collection from the keystore.
    /// </summary>
    internal class KeyCollectionService : IKeyCollectionService
    {
        #region Private Members
        private IKeystoreEncryptionService encryptionService;
        private IFileRepository fileRepository;
        string fileName = Config.KeystoreFileName;
        static private ILogger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region IKeyCollectionService Members

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="encryptionService">Service to encrypt credentials and colletction.</param>
        /// <param name="fileRepository">Repository to access, modify and delete the keystore.</param>
        public KeyCollectionService(IKeystoreEncryptionService encryptionService, IFileRepository fileRepository)
        {
            this.encryptionService = encryptionService;
            this.fileRepository = fileRepository;
        }

        /// <summary>
        /// Initializes a key collection.
        /// </summary>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public bool InitializeKeyCollection()
        {
            var emptyKeyStore = new KeyCollection();
            return this.WriteKeyCollection(emptyKeyStore);
        }

        /// <summary>
        /// Inserts a keystore entry into the key collection.
        /// </summary>
        /// <typeparam name="T">a type of keystore entry</typeparam>
        /// <param name="keystoreEntry">Keystore entry with credentials for the key collection.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public bool Insert<T>(T keystoreEntry) where T : IKeystoreEntry
        {
            if (keystoreEntry == null)
            {
                logger.Error(string.Format("Entry is null...returning"));
                throw new NullReferenceException("Entry is null...returning");
            }

            var keyCollection = this.GetKeyCollection();
            var sameAliasEntry = keyCollection.KeyStoreEntries.Where(k => k.Alias == keystoreEntry.Alias).FirstOrDefault();
            if (sameAliasEntry != null &&
                keyCollection.KeyStoreEntries.Count() > 0)
            {
                logger.Error(string.Format("Entry with alias: {0} already exists in keystore", keystoreEntry.Alias));
                throw new ArgumentException(string.Format("Entry with alias: {0} already exists in keystore", keystoreEntry.Alias));

            }
            keyCollection.KeyStoreEntries.Add(keystoreEntry);


            return WriteKeyCollection(keyCollection);

        }

        /// <summary>
        /// Retrieves the credentials found in the keystore with the provided alias.
        /// </summary>
        /// <typeparam name="T">A type of keystore entry.</typeparam>
        /// <param name="alias">Alias needed to retrieve credentials associated to it from keystore.</param>
        /// <returns>keystore entry populated with proper credentials retrieved from keystore.</returns>
        public T Get<T>(string alias) where T : IKeystoreEntry
        {
            EnsureArg.IsNotNullOrWhiteSpace(alias);
            KeyCollection keyCollection = this.GetKeyCollection();
            var result = keyCollection.KeyStoreEntries.Where(x => x.Alias == alias);
            return result != null && result.Count() == 1 ? (T)result.First() : default(T);
        }

        /// <summary>
        /// Removes from keystore, the credentials associated with the alias provided.
        /// </summary>
        /// <typeparam name="T">A type of keystore entry.</typeparam>
        /// <param name="alias">Alias needed to retrieve credentials associated to it from keystore.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public bool Remove<T>(string alias) where T : IKeystoreEntry
        {
            EnsureArg.IsNotNullOrWhiteSpace(alias);
            var keyCollection = this.GetKeyCollection();
            var result = keyCollection.KeyStoreEntries.Where(x => x.Alias == alias);

            if (result != null && result.Count() == 1)
            {
                keyCollection.KeyStoreEntries.Remove(result.FirstOrDefault());
                return this.WriteKeyCollection(keyCollection);
            }
            else if (result == null)
            {
                logger.Error(string.Format("Key does not exist under the alias: {0}", alias));
                throw new Exception(string.Format("Key does not exist under the alias: {0}", alias));
            }
            return false;
        }

        /// <summary>
        /// Updates the credentials associated with the provided alias with the keystore entry provided.
        /// </summary>
        /// <typeparam name="T">A type of keystore entry.</typeparam>
        /// <param name="alias">Alias needed to retrieve credentials associated to it from keystore.</param>
        /// <param name="keystoreEntry">Keystore entry with credentials for the key collection.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public bool Update<T>(string alias, T keystoreEntry) where T : IKeystoreEntry
        {

            var keyCollection = this.GetKeyCollection();
            var result = keyCollection.KeyStoreEntries.Where(x => x.Alias == alias);

            if (result != null && result.Count() == 1)
            {
                keyCollection.KeyStoreEntries.Remove(result.FirstOrDefault());
                keyCollection.KeyStoreEntries.Add(keystoreEntry);
            }
            return false;
        }

        /// <summary>
        /// Inserts a certificate keystore entry into the key collection.
        /// </summary>
        /// <typeparam name="T">A type of certificate keystore entry.</typeparam>
        /// <param name="certificateFileLocation">Path where certificate is located.</param>
        /// <param name="keystoreEntry">Keystore entry with credentials for the key collection.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public bool InsertCertificate<T>(
            string certificateFileLocation,
            T certificateToken) where T : IKeystoreEntry
        {
            if (certificateToken.Type != TokenKeyType.CERTIFICATE)
            {
                logger.Error(string.Format("Keystore entry with alias ({0})  is not certificate type entry", certificateToken.Alias));
                throw new Exception(string.Format("Keystore entry with alias ({0})  is not certificate type entry", certificateToken.Alias));
            }
            string certificateData = GetCertificateData(certificateFileLocation);

            (certificateToken as CertificateToken).RawData =
                Encoding.ASCII.GetBytes(certificateData);

            return this.Insert(certificateToken);
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Writes encrypted key collection to the keystore and updates cache.
        /// </summary>
        /// <param name="keyCollection">Up to date key collection.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        private bool WriteKeyCollection(KeyCollection keyCollection)
        {
            var encryptedData = this.encryptionService.EncryptKeyStore(keyCollection);
            var filePath = Path.Combine(ExecutingAssemblyHelper.GetExecutingAssemblyLocation(), this.fileName);
            if (this.fileRepository.Write(filePath, encryptedData))
            {
                CollectionCache.UpdateCache(encryptedData);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Retrieves a key collection from the keystore.
        /// </summary>
        /// <returns>Key collection returned from the keystore.</returns>
        private KeyCollection GetKeyCollection()
        {
            string encryptedData = string.Empty;

            if (CollectionCache.TryGetCollection(out encryptedData))
            {
                return this.encryptionService.DecryptKeyStore(encryptedData);
            }

            
            var filePath = Path.Combine(ExecutingAssemblyHelper.GetExecutingAssemblyLocation(), this.fileName);
            encryptedData = this.fileRepository.Read(filePath);
            CollectionCache.UpdateCache(encryptedData);
            return this.encryptionService.DecryptKeyStore(encryptedData);
        }

        /// <summary>
        /// Given an certificate location to fetch the string value of the certificate.
        /// </summary>
        /// <param name="certificateFileLocation">Path where the certificate is located.</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>

        private string GetCertificateData(string certificateFileLocation)
        {

            return this.fileRepository.Read(certificateFileLocation);
        }

        #endregion


    }
}
