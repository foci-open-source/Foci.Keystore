﻿using System;
using System.Linq;
using System.Security.Cryptography;
using FociSolutions.KeyStore.Models;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace FociSolutions.KeyStore.Configs
{
    /// <summary>
    /// Retrieves the configuration properties to set up the keystore. Directory name and location, data protection Scope, Entropy
    /// </summary>
    internal class Config
    {
        private static readonly SettingsManager settingsMgr = new SettingsManager();

        /// <summary>
        /// Keystore's Directory Location
        /// </summary>
        public static string KeystoreFileName
        {
            get
            {
                return settingsMgr.GetAppSetting<string>("KeystoreFileName", "my.keystore");
            }
        }
        /// <summary>
        /// Keystore's data protection scope for encrypting and decrypting credentials
        /// </summary>
        public static DataProtectionScope KeystoreDataProtectionScope
        {
            get
            {
                return settingsMgr.GetAppSetting<DataProtectionScope>("KeystoreDataProtectionScope", DataProtectionScope.CurrentUser);
            }
        }

        /// <summary>
        /// Part of the encryption of credentials
        /// </summary>
        public static string Entropy
        {
            get
            {
               return settingsMgr.GetAppSetting<string>("Entropy", "7,8,9");
             
            }
        }


    }
}
