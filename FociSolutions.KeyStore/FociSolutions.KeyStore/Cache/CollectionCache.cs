﻿using System;
using System.Runtime.Caching;
using System.Runtime.CompilerServices;

namespace FociSolutions.KeyStore.Cache
{
    /// <summary>
    /// Class that handles the collection's cache
    /// </summary>
    internal class CollectionCache
    {

        #region Private Members
        private static readonly MemoryCache collectionCache = new MemoryCache("Collection");
        #endregion

        #region Public Members
        /// <summary>
        /// Static Try Get. Should only every be false if the timer has expired.
        /// <returns> True if found, false if not found.</returns>
        public static bool TryGetCollection(out string keystore)
        {

            if (collectionCache.Contains("keystore"))
            {
                keystore = (string)collectionCache.Get("keystore");
                return true;
            }

            keystore = null;
            return false;
        }

        /// <summary>
        /// Overwrite the cache with a newer collection 
        /// </summary>
        /// <returns> the loaded cache</returns>
        public static void UpdateCache(string collection)
        {
            collectionCache.Set("keystore", collection, DateTimeOffset.UtcNow.AddDays(1));

        }

        /// <summary>
        /// Clear the cache from any keystore values.
        /// </summary>
        public static void ClearCache()
        {
            collectionCache.Remove("keystore");
        }
    }

    #endregion
}

