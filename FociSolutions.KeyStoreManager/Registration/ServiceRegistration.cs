﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using FociSolutions.KeyStoreManager.Configs;
using FociSolutions.KeyStoreManager.Models;
using FociSolutions.KeyStoreManager.Repositories;
using FociSolutions.KeyStoreManager.Repositories.Interface;
using FociSolutions.KeyStoreManager.Services;
using FociSolutions.KeyStoreManager.Services.Interface;

namespace FociSolutions.KeyStoreManager.Registration
{
    /// <summary>
    /// Service Registration class. Resolves current dependencies for inilitialization of services.
    /// </summary>
    public class ServiceRegistration
    {
        private bool isCollectionServiceRegistered = false;
        private bool isCertificateServiceRegistered = false;
        private IUnityContainer containerKeyCollectionSrvice;
        private IUnityContainer containerCertificateService;

        #region ServiceRegistration Members
        /// <summary>
        /// Register Types in container for Key Collection Service. If already has registered dependencies, it will resolve and return KeyCollection.
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public BaseKeyCollectionService RegisterKeyCollectionServiceDependencies(IUnityContainer container) {
            if (isCollectionServiceRegistered) {
                return containerKeyCollectionSrvice.Resolve<BaseKeyCollectionService>();
            }
            container.RegisterType<IKeystoreEncryptionService, KeystoreEncryptionService>();
            container.RegisterType<IFileRepository, FileRepository>();
            container.RegisterType<IBaseKeyCollectionService, BaseKeyCollectionService>();
            isCollectionServiceRegistered = true;
            this.containerKeyCollectionSrvice = container;
            return container.Resolve<BaseKeyCollectionService>();

        }
        /// <summary>
        /// Register Types in container for Certificate Service. If already has registered dependencies, it will resolve and return Certificate Service.
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public CertificateService RegisterCertificateServiceDependencies(IUnityContainer container)
        {
            if (IsCertificateServiceRegistered) {
                return containerCertificateService.Resolve<CertificateService>();
            }
            container.RegisterType<IKeystoreEncryptionService, KeystoreEncryptionService>();
            container.RegisterType<IFileRepository, FileRepository>();
            container.RegisterType<ICertificateService, CertificateService>();
            isCertificateServiceRegistered = true;
            containerCertificateService = container;
            return container.Resolve<CertificateService>();

        }
        /// <summary>
        /// Flag to determine if Key Collection Service and Dependencies are registered.
        /// </summary>
        public bool IsCollectionServiceRegistered {
            get {
                return isCollectionServiceRegistered;
            }
        }
        /// <summary>
        /// Flag to determine if Certificate Service and Dependencies are registered.
        /// </summary>
        public bool IsCertificateServiceRegistered {
            get {
                return isCertificateServiceRegistered;
            }
        }
        #endregion
    }
}
