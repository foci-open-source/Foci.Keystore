﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using NLog;
using FociSolutions.KeyStoreManager.Configs;
using FociSolutions.KeyStoreManager.Models;
using FociSolutions.KeyStoreManager.Services.Interface;
using Newtonsoft.Json;
using EnsureThat;
using System.Runtime.Serialization;

namespace FociSolutions.KeyStoreManager.Services
{
    public class KeystoreEncryptionService : IKeystoreEncryptionService
    {
        #region IKeystoreEncryptionService Private Members
        static private byte[] Entropy { get; set; }
        private readonly DataProtectionScope ProtectionScope = Config.KeystoreDataProtectionScope;
        static private readonly byte[] Entropy1 = new byte[] { 1, 2, 3 };
        static private readonly string Entropy2 = Config.Entropy;
        static private readonly ILogger logger = LogManager.GetCurrentClassLogger();
        #endregion

        #region IKeystoreEncryptionService Members

        public KeystoreEncryptionService()
        {
            EntropyBuild();
        }

        public KeyCollection DecryptKeyStore(string KeyStoreEncryptedData)
        {
            EnsureArg.IsNotNullOrWhiteSpace(KeyStoreEncryptedData);
            var decryptedData = Decrypt(KeyStoreEncryptedData, Entropy, this.ProtectionScope);
            var settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All
            };
            KeyCollection keyCollection = JsonConvert.DeserializeObject<KeyCollection>(decryptedData, settings);
            return keyCollection;
        }

        public string EncryptKeyStore(KeyCollection KeyStore)
        {
            EnsureArg.IsNotNull(KeyStore);

            var settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All
            };

            var jsonData = JsonConvert.SerializeObject(KeyStore, Formatting.Indented, settings);
            var encryptedData = Encrypt(jsonData, Entropy, this.ProtectionScope);

            return encryptedData;
        }

        #endregion

        #region Private Methods
        private static void EntropyBuild() {
            var parts = Entropy2.Split(new[] { ',' });
            byte[] Entropy2ByteArray = Array.ConvertAll(parts, p => byte.Parse(p));
            Entropy = new byte[Entropy1.Length + Entropy2.Length];
            Buffer.BlockCopy(Entropy1, 0, Entropy, 0, Entropy1.Length);
            Buffer.BlockCopy(Entropy2ByteArray, 0, Entropy, Entropy1.Length, Entropy2ByteArray.Length);
           

        }
        private static string Encrypt(
            string text,
            byte[] entropy,
            DataProtectionScope dataProtectionScope)
        {
            EnsureArg.IsNotNullOrWhiteSpace(text);
        
            byte[] originalText = Encoding.Unicode.GetBytes(text);
         
            byte[] encryptedText = ProtectedData.Protect(
                originalText, entropy, dataProtectionScope);
            
            return Convert.ToBase64String(encryptedText);
        }

        private static string Decrypt(
            string text,
            byte[] entropy,
            DataProtectionScope dataProtectionScope)
        {
            EnsureArg.IsNotNullOrWhiteSpace(text);
            try
            {
                byte[] encryptedText = Convert.FromBase64String(text);

                byte[] originalText = ProtectedData.Unprotect(
                    encryptedText, entropy, dataProtectionScope);
                
                return Encoding.Unicode.GetString(originalText);
            }
            catch (CryptographicException c) {
                logger.Error("Source: {0}; Message {1}; Type {2}.", c.Source, c.Message, c.GetType().ToString());
                logger.Trace("Exception Stack Trace: {0}", c.StackTrace);
                throw;
            }
        }

        #endregion
    }
}
