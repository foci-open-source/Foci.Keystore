﻿using FociSolutions.KeyStoreManager.Models;

namespace FociSolutions.KeyStoreManager.Services.Interface
{
    public interface IKeystoreEncryptionService
    {
        KeyCollection DecryptKeyStore(string KeyStoreEncryptedData);
        string EncryptKeyStore(KeyCollection KeyStore);

    }
}
