﻿using FociSolutions.KeyStoreManager.Models;

namespace FociSolutions.KeyStoreManager.Services.Interface
{
    public interface ICertificateService : IBaseKeyCollectionService
    {
        bool InsertCertificate<T>(string certificateFileLocation, T keystoreEntry) where T : IKeystoreEntry;
    }
}