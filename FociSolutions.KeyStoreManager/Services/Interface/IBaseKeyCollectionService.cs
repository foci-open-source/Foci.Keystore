﻿using FociSolutions.KeyStoreManager.Models;

namespace FociSolutions.KeyStoreManager.Services.Interface
{
    public interface IBaseKeyCollectionService
    {
        bool InitializeKeyCollection();
        bool Insert<T>(T keystoreEntry) where T : IKeystoreEntry;
        T Get<T>(string alias) where T : IKeystoreEntry;
        bool Delete<T>(string alias) where T : IKeystoreEntry;
        bool Update<T>(string alias, T keystoreEntry) where T : IKeystoreEntry;

    }
}