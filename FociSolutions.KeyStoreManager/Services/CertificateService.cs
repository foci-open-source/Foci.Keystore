﻿using FociSolutions.KeyStoreManager.Configs;
using FociSolutions.KeyStoreManager.Models;
using FociSolutions.KeyStoreManager.Repositories.Interface;
using System;
using System.IO;
using System.Text;

namespace FociSolutions.KeyStoreManager.Services.Interface
{
    public class CertificateService : BaseKeyCollectionService, ICertificateService
    {
        private readonly IKeystoreEncryptionService encryptionService;
        private readonly IFileRepository fileRepository;

        #region IKeyCollectionService Members

        /// <summary>
        /// Initialize Certificate service with encryptionService,
        /// and fileRepository interface.
        /// </summary>
        /// <param name="encryptionService"></param>
        /// <param name="fileRepository"></param>
        public CertificateService(
            IKeystoreEncryptionService encryptionService,
            IFileRepository fileRepository) :
            base(encryptionService,fileRepository)
        {
            this.encryptionService = encryptionService;
            this.fileRepository = fileRepository;
        }

        /// <summary>
        /// Insert a certificate file to certificate token.
        /// </summary>
        /// <typeparam name="T">CertificateToken</typeparam>
        /// <param name="certificateFileLocation"></param>
        /// <param name="certificateToken"></param>
        /// <returns>Boolean value to indicate the success insert or not.</returns>
        public bool InsertCertificate<T>(
            string certificateFileLocation,
            T certificateToken) where T : IKeystoreEntry
        {
            string certificateData = GetCertificateData(certificateFileLocation);

            (certificateToken as CertificateToken).RawData =
                Encoding.ASCII.GetBytes(certificateData);

            return this.Update(certificateToken.Alias, certificateToken);
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Given an certificate location to fetch the string value of the certificate.
        /// </summary>
        /// <param name="certificateFileLocation"></param>
        /// <returns>Boolean value to indicate the success read or not</returns>
        private string GetCertificateData(string certificateFileLocation)
        {
            return this.fileRepository.Read(certificateFileLocation);
        }

        #endregion
    }
}
