﻿using EnsureThat;
using FociSolutions.KeyStoreManager.Configs;
using FociSolutions.KeyStoreManager.Models;
using FociSolutions.KeyStoreManager.Repositories.Interface;
using FociSolutions.KeyStoreManager.Services.Interface;
using NLog;
using System;
using System.Linq;

namespace FociSolutions.KeyStoreManager.Services
{
    public class BaseKeyCollectionService : IBaseKeyCollectionService
    {
        #region Private Members
        private IKeystoreEncryptionService encryptionService;
        private IFileRepository fileRepository;
        string FilePath = Config.KeystoreDirectory;
        TokenKeyType type = Config.TokenType;
        static private ILogger logger = LogManager.GetCurrentClassLogger();
        #endregion

        #region IKeyCollectionService Members

        public BaseKeyCollectionService(IKeystoreEncryptionService encryptionService, IFileRepository fileRepository)
        {
            this.encryptionService = encryptionService;
            this.fileRepository = fileRepository;
        }

        public bool InitializeKeyCollection()
        {
            var emptyKeyStore = new KeyCollection();
            return this.WriteKeyCollection(emptyKeyStore);
        }

        public bool Insert<T>(T keystoreEntry) where T : IKeystoreEntry
        {
            if (keystoreEntry == null)
            {
                logger.Error(string.Format("Entry is null...returning"));
                throw new NullReferenceException("Entry is null...returning");
            }

            var keyCollection =  this.GetKeyCollection(); 
            var sameAliasEntry = keyCollection.KeyStoreEntries.Where(k => k.Alias == keystoreEntry.Alias).FirstOrDefault();
            if (sameAliasEntry != null &&
                keyCollection.KeyStoreEntries.Count() > 0)
            {
                logger.Error(string.Format("Entry with alias: {0} already exists in keystore",keystoreEntry.Alias));
                throw new ArgumentException(string.Format("Entry with alias: {0} already exists in keystore", keystoreEntry.Alias));
               
            }
            keyCollection.KeyStoreEntries.Add(keystoreEntry);

            return this.WriteKeyCollection(keyCollection);
        }

        public T Get<T>(string alias) where T : IKeystoreEntry
        {
            EnsureArg.IsNotNullOrWhiteSpace(alias);
            KeyCollection keyCollection = this.GetKeyCollection(); 
            var result = keyCollection.KeyStoreEntries.Where(x => x.Alias == alias);
            return result != null && result.Count() == 1 ? (T)result.First() : default(T);
        }

        public bool Delete<T>(string alias) where T : IKeystoreEntry
        {
            EnsureArg.IsNotNullOrWhiteSpace(alias);
            var keyCollection = this.GetKeyCollection(); 
            var result = keyCollection.KeyStoreEntries.Where(x => x.Alias == alias);

            if (result != null && result.Count() == 1)
            {
                keyCollection.KeyStoreEntries.Remove(result.FirstOrDefault());
                return this.WriteKeyCollection( keyCollection);
            }
            return false;
        }

        public bool Update<T>(string alias, T keystoreEntry) where T : IKeystoreEntry
        {
            
            var keyCollection = this.GetKeyCollection(); 
            var result = keyCollection.KeyStoreEntries.Where(x => x.Alias == alias);

            if (result != null && result.Count() == 1)
            {
                keyCollection.KeyStoreEntries.Remove(result.FirstOrDefault());
                return this.WriteKeyCollection(keyCollection);
            }
            return false;
        }

        #endregion

        #region Private Methods
        private bool WriteKeyCollection(KeyCollection keyCollection)
        {
            var encryptedData = this.encryptionService.EncryptKeyStore(keyCollection);

            return this.fileRepository.Write(this.FilePath, encryptedData);
        }
        
        private KeyCollection GetKeyCollection()
        {
            var encryptedData = this.fileRepository.Read(this.FilePath);
            var KeyCollection = this.encryptionService.DecryptKeyStore(encryptedData);

            return KeyCollection;
        }

        #endregion

    }
}
