using System.Text;
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FociSolutions.KeyStoreManager.Models;
namespace FociSolutions.KeyStoreManager.Repositories.Interface
{
    public interface IFileRepository
    {
        bool Create(string fileLocation);

        bool Write(string fileLocation, string keyCollectionContent);

        string Read(string fileLocation);

        bool Append(string fileLocation, string keyCollectionContent);

        bool Delete(string fileLocation);
    }
}
