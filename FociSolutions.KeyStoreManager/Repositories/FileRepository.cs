﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FociSolutions.KeyStoreManager.Models;
using NLog;
using FociSolutions.KeyStoreManager.Repositories.Interface;
using System.IO;

namespace FociSolutions.KeyStoreManager.Repositories
{
    public class FileRepository : IFileRepository
    {
        #region Private Members
        static private ILogger logger = LogManager.GetCurrentClassLogger();
        #endregion
        #region IFileRepository Members 

        public FileRepository()
        {

        }

        public bool Create(string fileLocation)
        {

            if (File.Exists(fileLocation))
            {
                logger.Warn(string.Format("File already exists"));
                return true;
            }

            try
            {
                File.Create(fileLocation);
            }
            catch (IOException e)
            {
                logger.Error("Source: {0}; Message {1}; Type {2}.", e.Source, e.Message, e.GetType().ToString());
                logger.Trace("IO Exception Stack Trace: {0}", e.StackTrace);
                logger.Error(e);
                return false;
            }

            return true;
        }

        public string Read(string fileLocation)
        {
            if (!File.Exists(fileLocation))
            {
                logger.Warn(string.Format("{0} does not exist",fileLocation));
                throw new ArgumentException(string.Format("{0} does not exist", fileLocation), "fileLocation");
            }

            var fileContent = File.ReadAllText(fileLocation);
            return fileContent;
        }

        public bool Write(string fileLocation, string fileContent)
        {
            try
            {
                File.WriteAllText(fileLocation, fileContent);
            }
            catch (IOException e)
            {
                logger.Error("Source: {0}; Message {1}; Type {2}.", e.Source, e.Message, e.GetType().ToString());
                logger.Trace("IO Exception Stack Trace: {0}", e.StackTrace);
                logger.Error(e);
                return false;
            }

            return true;
        }

        public bool Append(string fileLocation, string keyCollectionContent)
        {
            try
            {
                File.AppendAllText(fileLocation, keyCollectionContent);
            }
            catch (IOException e)
            {
                logger.Error("Source: {0}; Message {1}; Type {2}.", e.Source, e.Message, e.GetType().ToString());
                logger.Trace("IO Exception Stack Trace: {0}", e.StackTrace);
                logger.Error(e);
                return false;
            }

            return true;
        }

        public bool Delete(string fileLocation)
        {

            if (!File.Exists(fileLocation))
            {
                logger.Warn(string.Format("{0} does not exist", fileLocation));
                throw new ArgumentException(string.Format("{0} does not exist", fileLocation), "fileLocation");
            }
            try
            {
                File.Delete(fileLocation);
            }
            catch (IOException e)
            {
                logger.Error("Source: {0}; Message {1}; Type {2}.", e.Source, e.Message, e.GetType().ToString());
                logger.Trace("IO Exception Stack Trace: {0}", e.StackTrace);
                logger.Error(e);
                return false;
            }

            return true;
        }
       
        #endregion
    }
}
