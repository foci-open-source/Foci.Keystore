﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace FociSolutions.KeyStoreManager
{
    class Program
    {
        public static int Main(string[] args)
        {
            var commands = GetCommands();

            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.WriteLine(@"The Keystore Manager debugger is running. Type command to interact with the keystore or type 'stop' to exit.");
                var argString = string.Empty;

                while (argString.ToLower() != "stop")
                {
                    argString = Console.ReadLine();
                    args = Regex.Matches(argString, @"[\""].+?[\""]|[^ ]+")
                                .Cast<Match>()
                                .Select(m => m.Value)
                                .ToArray();
                    ConsoleCommandDispatcher.DispatchCommand(commands, args, Console.Out);
                }
            }

            return ConsoleCommandDispatcher.DispatchCommand(commands, args, Console.Out);

        }

        public static IEnumerable<ConsoleCommand> GetCommands()
        {
            return ConsoleCommandDispatcher.FindCommandsInSameAssemblyAs(typeof(Program));
        }
    }
}
