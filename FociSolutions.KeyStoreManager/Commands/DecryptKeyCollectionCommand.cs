﻿//using FociSolutions.KeyStoreManager.Services;
//using FociSolutions.KeyStoreManager.Services.Interface;
//using ManyConsole;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace FociSolutions.KeyStoreManager.Commands
//{
//    public class DecryptKeyCollectionCommand : ConsoleCommand
//    {
//        public string KeystoreLocation { get; set; }
//        public string DecyptedLocation { get; set; }

//        public DecryptKeyCollectionCommand()
//        {
//            IsCommand("DecyptKeyCollection", "Writes a decrypted keystore to file");
//            HasLongDescription(@"This command is used to decypt a keystore to disk.");

//            HasRequiredOption("k|keystore=", "The full path of the keystore.", k => KeystoreLocation = k);
//            HasRequiredOption("d|decrypt=", "The full path of the decrypted keystore.", k => DecyptedLocation = k);
//        }

//        public override int Run(string[] remainingArguments)
//        {
//            try
//            {
//                IKeystoreEncryptionService keystoreEncryptionService = new KeystoreEncryptionService();
//                IKeyCollectionService keyCollectionService = new KeyCollectionService(keystoreEncryptionService);

//                keyCollectionService.DecryptKeyCollection(this.KeystoreLocation, this.DecyptedLocation);

//                return (int)CommandReturnResults.Success;

//            }
//            catch (Exception ex)
//            {
//                Console.Error.WriteLine(ex.Message);
//                Console.Error.WriteLine(ex.StackTrace);

//                return (int)CommandReturnResults.Failure;
//            }
//        }
//    }
//}
