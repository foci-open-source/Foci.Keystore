﻿using FociSolutions.KeyStore.Models;
using ManyConsole;
using System;
using FociSolutions.KeyStore;

namespace FociSolutions.KeyStoreManager.Commands
{
    /// <summary>
    /// Command to retrieve a certificate from the keystore.
    /// </summary>
    class GetCertificateCommand : ConsoleCommand
    {
        public string Alias { get; set; }

        /// <summary>
        /// Explains the purpose of the command and takes in the arguments.
        /// </summary>
        public GetCertificateCommand()
        {
            IsCommand("GetCertificate", "Gets certificate with alias provided");
            HasLongDescription(@"This command can be used to retrieve a certificate.");

            HasRequiredOption("a|alias=", "The alias for the certificate.", k => Alias = k);

        }

        /// <summary>
        /// Performs the actual operation to get a certificate from the keystore.
        /// </summary>
        /// <param name="remainingArguments">additional arguments</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public override int Run(string[] remainingArguments)
        {
            try
            {
                var certificate = Keystore.Get<IKeystoreEntry>(Alias);
                Console.WriteLine(certificate.ToString());
                return (int)CommandReturnResults.Success;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return (int)CommandReturnResults.Failure;
            }
        }
    }
}
