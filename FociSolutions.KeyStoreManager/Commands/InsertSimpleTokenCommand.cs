﻿using FociSolutions.KeyStore;
using FociSolutions.KeyStore.Models;
using ManyConsole;
using System;

namespace FociSolutions.KeyStoreManager.Commands
{
    /// <summary>
    /// Command to insert a simple token to the keystore.
    /// </summary>
    public class InsertSimpleTokenCommand : ConsoleCommand
    {
        public string Alias { get; set; }
        public string Token { get; set; }

        /// <summary>
        /// Explains the purpose of the command and takes in the arguments.
        /// </summary>
        public InsertSimpleTokenCommand()
        {
            IsCommand("InsertSimpleToken", "Adds the given alias and token to the provided keystore");
            HasLongDescription(@"This command can be used to insert a simple token to a provided store. 
                                 The store must first be created.");

            HasRequiredOption("a|alias=", "The alias for the simple token.", k => Alias = k);
            HasRequiredOption("t|token=", "The Token to insert to the keystore.", t => Token = t);

        }

        /// <summary>
        /// Performs the actual operation to insert a simple token to the keystore.
        /// </summary>
        /// <param name="remainingArguments">additional arguments</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public override int Run(string[] remainingArguments)
        {
            try
            {
                var SimpleToken = new UsernameToken()
                {
                    Alias = this.Alias,
                    Token = this.Token,
                    Type = TokenKeyType.TOKEN,
                    MetaData = new MetaData() { ExpiryTime = new DateTime() }
                };

                Keystore.Insert(SimpleToken);
                return (int)CommandReturnResults.Success;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return (int)CommandReturnResults.Failure;
            }
        }
    }
}
