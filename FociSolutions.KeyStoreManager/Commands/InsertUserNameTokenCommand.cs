﻿using FociSolutions.KeyStore.Models;
using ManyConsole;
using System;
using FociSolutions.KeyStore;

namespace FociSolutions.KeyStoreManager.Commands
{
    /// <summary>
    /// Command to insert a username token to the keystore.
    /// </summary>
    public class InsertUserNameTokenCommand : ConsoleCommand
    {
        public string Alias { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }

        /// <summary>
        /// Explains the purpose of the command and takes in the arguments.
        /// </summary>
        public InsertUserNameTokenCommand()
        {
            IsCommand("InsertUsernameToken", "Inserts the given username and token to the provided keystore");
            HasLongDescription(@"This command can be used to add a username and token to a provided store. 
                                 The store must first be created.");

            HasRequiredOption("a|alias=", "The alias for the username and token.", k => Alias = k);
            HasRequiredOption("u|username=", "The Username to insert.", u => UserName = u);
            HasRequiredOption("p|password=", "The Password to insert.", p => Password = p);
            HasRequiredOption("t|token=", "The Token to insert to the keystore.", t => Token = t);

        }

        /// <summary>
        /// Performs the actual operation to insert a username token to the keystore.
        /// </summary>
        /// <param name="remainingArguments">additional arguments</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public override int Run(string[] remainingArguments)
        {
            try
            {
                var userNameToken = new UsernameToken()
                {
                    Alias = this.Alias,
                    Token = this.Token,
                    UserName = this.UserName,
                    Password = this.Password
                };

                Keystore.Insert(userNameToken);
                return (int)CommandReturnResults.Success;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return (int)CommandReturnResults.Failure;
            }
        }
    }
}
