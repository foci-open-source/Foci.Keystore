﻿using FociSolutions.KeyStore;
using FociSolutions.KeyStore.Models;
using ManyConsole;
using System;
namespace FociSolutions.KeyStoreManager.Commands
{
    /// <summary>
    /// Command to retrieve a username token from the keystore.
    /// </summary>
    public class GetUsernameTokenCommand : ConsoleCommand
    {
        public string Alias { get; set; }

        /// <summary>
        /// Explains the purpose of the command and takes in the arguments.
        /// </summary>
        public GetUsernameTokenCommand()
        {
            IsCommand("GetUsernameToken", "Gets username token with alias provided");
            HasLongDescription(@"This command can be used to retrieve a username token.");

            HasRequiredOption("a|alias=", "The alias for the username token.", k => Alias = k);

        }

        /// <summary>
        /// Performs the actual operation to get a username token from the keystore.
        /// </summary>
        /// <param name="remainingArguments">additional arguments</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public override int Run(string[] remainingArguments)
        {
            try
            {
                var userName = Keystore.Get<IKeystoreEntry>(Alias);
                Console.WriteLine(userName.ToString());
                return (int)CommandReturnResults.Success;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return (int)CommandReturnResults.Failure;
            }
        }
    }
}
