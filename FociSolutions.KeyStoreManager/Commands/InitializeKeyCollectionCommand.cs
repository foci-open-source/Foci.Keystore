﻿using ManyConsole;
using System;
using FociSolutions.KeyStore;

namespace FociSolutions.KeyStoreManager.Commands
{
    /// <summary>
    /// Command to initialize an empty keystore.
    /// </summary>
    public class InitializeKeyCollectionCommand : ConsoleCommand
    {
        /// <summary>
        /// Explains the purpose of the command and takes in the arguments.
        /// </summary>
        public InitializeKeyCollectionCommand()
        {
            IsCommand("InitializeKeyStore", "Creates an empty encrypted keystore");
            HasLongDescription(@"This command is used to create a new keystore.
                                 This command is required before using other keystore commands.");
        }

        /// <summary>
        /// Performs the actual operation to initialize an empty keystore.
        /// </summary>
        /// <param name="remainingArguments">additional arguments</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public override int Run(string[] remainingArguments)
        {
            try
            {
                Keystore.Initialize();
                return (int)CommandReturnResults.Success;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return (int)CommandReturnResults.Failure;
            }
        }
    }
}
