﻿using FociSolutions.KeyStore.Models;
using ManyConsole;
using System;
using FociSolutions.KeyStore;

namespace FociSolutions.KeyStoreManager.Commands
{
    /// <summary>
    /// Command to insert a certificate to the keystore.
    /// </summary>
    public class InsertCertificateCommand : ConsoleCommand
    {
        public string Alias { get; set; }
        public string CertificateLocation { get; set; }

        /// <summary>
        /// Explains the purpose of the command and takes in the arguments.
        /// </summary>
        public InsertCertificateCommand()
        {
            IsCommand("InsertCertificate", "Inserts the given certificate to the provided keystore");
            HasLongDescription(@"This command can be used to insert a certificateto a provided store. 
                                 The store must first be created.");

            HasRequiredOption("a|alias=", "The alias for the certificate.", k => Alias = k);
            HasRequiredOption("c|certificateLocation=", "The certificates filepath to insert.", u => CertificateLocation = u);

        }

        /// <summary>
        /// Performs the actual operation to insert a certificate to the keystore.
        /// </summary>
        /// <param name="remainingArguments">additional arguments</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public override int Run(string[] remainingArguments)
        {
            try
            {
                var certificateToken = new CertificateToken()
                {
                    Alias = this.Alias,
                };
                Keystore.InsertCertificate(this.CertificateLocation, certificateToken);
                return (int)CommandReturnResults.Success;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return (int)CommandReturnResults.Failure;
            }
        }
    }
}
