﻿using FociSolutions.KeyStore;
using FociSolutions.KeyStore.Models;
using ManyConsole;
using System;

namespace FociSolutions.KeyStoreManager.Commands
{
    /// <summary>
    /// Command to insert a keypair to the keystore.
    /// </summary>
    public class InsertKeyPairCommand : ConsoleCommand
    {
        public string Alias { get; set; }
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }

        /// <summary>
        /// Explains the purpose of the command and takes in the arguments.
        /// </summary>
        public InsertKeyPairCommand()
        {
            IsCommand("InsertKeyPair", "Adds the given alias, public key, and private key to the provided keystore");
            HasLongDescription(@"This command can be used to insert a keypair to a provided store. 
                                 The store must first be created.");

            HasRequiredOption("a|alias=", "The alias for the simple token.", k => Alias = k);
            HasRequiredOption("pu|public=", "The public key to insert to the keystore.", p => PublicKey = p);
            HasRequiredOption("pr|private=", "The private key to insert to the keystore.", pr => PrivateKey = pr);

        }

        /// <summary>
        /// Performs the actual operation to insert a keypair to the keystore.
        /// </summary>
        /// <param name="remainingArguments">additional arguments</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public override int Run(string[] remainingArguments)
        {
            try
            {
                var SimpleToken = new KeyPair()
                {
                    Alias = this.Alias,
                    PrivateKey = new PrivateKey() { KeyContent = this.PrivateKey },
                    PublicKey = new PublicKey() { KeyContent = this.PublicKey },
                    Type = TokenKeyType.KEYPAIR,
                    MetaData = new MetaData() { ExpiryTime = new DateTime() }
                };

                Keystore.Insert(SimpleToken);
                return (int)CommandReturnResults.Success;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return (int)CommandReturnResults.Failure;
            }
        }
    }
}
