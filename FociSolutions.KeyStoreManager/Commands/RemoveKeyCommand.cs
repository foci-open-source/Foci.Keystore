﻿using FociSolutions.KeyStore;
using FociSolutions.KeyStore.Models;
using ManyConsole;
using System;

namespace FociSolutions.KeyStoreManager.Commands
{
    /// <summary>
    /// Command to remove a key entry from the keystore.
    /// </summary>
    public class RemoveKeyCommand : ConsoleCommand
    {
        public string Alias { get; set; }

        /// <summary>
        /// Explains the purpose of the command and takes in the arguments.
        /// </summary>
        public RemoveKeyCommand()
        {
            IsCommand("RemoveKey", "Remove tkey by passing the alias to a provided keystore");
            HasLongDescription(@"This command can be used to remove a key to a provided store. 
                                 The store must first be created.");

            HasRequiredOption("a|alias=", "The alias for the key.", k => Alias = k);


        }

        /// <summary>
        /// Performs the actual operation to remove a key entry from the keystore.
        /// </summary>
        /// <param name="remainingArguments">additional arguments</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public override int Run(string[] remainingArguments)
        {
            try
            {
                Keystore.Remove<IKeystoreEntry>(Alias);
                return (int)CommandReturnResults.Success;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return (int)CommandReturnResults.Failure;
            }
        }
    }
}
