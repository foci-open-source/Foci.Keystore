﻿using FociSolutions.KeyStore.Models;
using ManyConsole;
using System;
using FociSolutions.KeyStore;

namespace FociSolutions.KeyStoreManager.Commands
{
    /// <summary>
    /// Command to retrieve a key pair from the keystore.
    /// </summary>
    class GetKeypairCommand : ConsoleCommand
    {
        public string Alias { get; set; }

        /// <summary>
        /// Explains the purpose of the command and takes in the arguments.
        /// </summary>
        public GetKeypairCommand()
        {
            IsCommand("GetKeypair", "Gets key pair with alias provided");
            HasLongDescription(@"This command can be used to retrieve a key pair.");

            HasRequiredOption("a|alias=", "The alias for the key pair.", k => Alias = k);

        }

        /// <summary>
        /// Performs the actual operation to get a keypair from the keystore.
        /// </summary>
        /// <param name="remainingArguments">additional arguments</param>
        /// <returns>Flag to indicate if process completation was successful or failed.</returns>
        public override int Run(string[] remainingArguments)
        {
            try
            {
                var keyPair = Keystore.Get<IKeystoreEntry>(Alias);
                Console.WriteLine(keyPair.ToString());
                return (int)CommandReturnResults.Success;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return (int)CommandReturnResults.Failure;
            }
        }
    }
}
