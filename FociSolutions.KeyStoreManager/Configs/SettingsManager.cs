﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;

namespace FociSolutions.KeyStoreManager.Configs
{
    /// <summary>
    /// Adds strong typing and Enum support to System.Configuration.ConfigurationManager
    /// </summary>
    public class SettingsManager
    {

        /// <summary>
        /// Gets the config property with the specified name from the AppSettings.
        /// </summary>
        /// <param name="propName">Name of the property to get.</param>
        /// <param name="defaultValue">Default value to use if no property is found.</param>
        /// <param name="allowNull">if true, allows the property value to be null.</param>
        /// <typeparam name="T">Type of property to return.</typeparam>
        /// <returns>The AppSetting property.</returns>
        public T GetAppSetting<T>(string propName, T defaultValue, bool allowNull)
        {
            var protoProp = ConfigurationManager.AppSettings[propName];
            if (protoProp == null)
            {
                if (!allowNull && object.Equals(defaultValue, null))
                {
                    throw new ArgumentNullException(string.Format("AppSetting {0} cannot be null.", propName));
                }

                return defaultValue;
            }

            try
            {
                return ValueConverter.ConvertValue<T>(protoProp, defaultValue);
            }
            catch (FormatException fe)
            {
                throw new ConfigurationErrorsException(string.Format("AppSetting {0} cannot be cast to type {1}", propName, typeof(T).Name), fe);
            }
            catch (InvalidCastException ice)
            {
                throw new ConfigurationErrorsException(string.Format("AppSetting {0} cannot be cast to type {1}", propName, typeof(T).Name), ice);
            }
            catch (ArgumentNullException ane)
            {
                throw new ConfigurationErrorsException(string.Format("AppSetting {0} cannot be null.", propName), ane);
            }
            catch (ArgumentException ae)
            {
                throw new ConfigurationErrorsException(string.Format("Invalid value [{0}] for AppSeting {1}", protoProp, propName), ae);
            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException(string.Format("Error reading AppSetting {0}", propName), ex);
            }
        }

        /// <summary>
        /// Gets the config property with the specified name from the AppSettings.
        /// </summary>
        /// <param name="propName">Name of the property to get.</param>
        /// <param name="defaultValue">Default value to use if no property is found.</param>
        /// <typeparam name="T">Type of property to return.</typeparam>
        /// <returns>The AppSetting property.</returns>
        public T GetAppSetting<T>(string propName, T defaultValue)
        {
            return GetAppSetting(propName, defaultValue, false);
        }

    }

    internal static class ValueConverter
    {
        /// <summary>
        /// strings to T conversion
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns>T</returns>
        public static T ConvertValue<T>(string value, T defaultValue)
        {
            if (defaultValue != null && defaultValue is Enum)
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }

            return (T)Convert.ChangeType(value, typeof(T));
        }


    }
}
