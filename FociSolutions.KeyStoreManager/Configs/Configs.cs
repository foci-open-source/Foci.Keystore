﻿using System;
using System.Linq;
using System.Security.Cryptography;
﻿using FociSolutions.KeyStoreManager.Models;
using System.Configuration;

namespace FociSolutions.KeyStoreManager.Configs
{
    internal class Config
    {
        private static readonly SettingsManager settingsMgr = new SettingsManager();

        public static string KeystoreDirectory
        {
            get
            {
                return settingsMgr.GetAppSetting<string>("KeystoreDirectory", "./test.keystore");
            }
        }
        public static DataProtectionScope KeystoreDataProtectionScope
        {
            get
            {
                return settingsMgr.GetAppSetting<DataProtectionScope>("KeystoreDataProtectionScope", DataProtectionScope.CurrentUser);
            }
        }

        public static string Entropy
        {
            get
            {
               return settingsMgr.GetAppSetting<string>("Entropy", "7,8,9");
             
            }
        }

        public static TokenKeyType TokenType
        {
            get
            {
                return settingsMgr.GetAppSetting<TokenKeyType>("TokenType", TokenKeyType.KEYPAIR);
            }
        }

    }
}
