﻿
namespace FociSolutions.KeyStoreManager.Models
{
    public class SimpleToken : KeystoreEntry
    {
        public string Token { get; set; }
    }

}
