﻿using System.Collections.Generic;

namespace FociSolutions.KeyStoreManager.Models
{
    public class KeyCollection
    {
        public KeyCollection()
        {

            this.KeyStoreEntries = new List<IKeystoreEntry>();
        }

        public IList<IKeystoreEntry> KeyStoreEntries { get; set; }
    }
}
