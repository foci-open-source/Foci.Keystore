﻿using FociSolutions.KeyStoreManager.Models;


namespace FociSolutions.KeyStoreManager.Models
{
    public class KeyPair : KeystoreEntry
    { 
        public PublicKey PublicKey { get; set; }
        public PrivateKey PrivateKey { get; set; }
        public bool IsCert { get; set; }
    }
}
