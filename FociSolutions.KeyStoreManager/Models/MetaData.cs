﻿using System;

namespace FociSolutions.KeyStoreManager.Models
{
    public class MetaData
    {
        public DateTime ExpiryTime { get; set; }
    }
}
