﻿
namespace FociSolutions.KeyStoreManager.Models
{
    public class CertificateToken : KeystoreEntry
    {
        public byte[] RawData { get; set; }
    }
}
