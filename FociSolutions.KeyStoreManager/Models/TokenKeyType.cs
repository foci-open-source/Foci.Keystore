﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FociSolutions.KeyStoreManager.Models
{
    public enum TokenKeyType
    {
        KEYPAIR = 0,
        TOKEN,
        USERNAMETOKEN
    }

}
