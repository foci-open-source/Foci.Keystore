﻿
namespace FociSolutions.KeyStoreManager.Models
{
    public interface IKeystoreEntry
    {
        string Alias { get; set; }
        TokenKeyType Type { get; set; }
        MetaData MetaData { get; set; }

    }
}
