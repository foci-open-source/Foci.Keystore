﻿
namespace FociSolutions.KeyStoreManager.Models
{
    public class UsernameToken : KeystoreEntry
    {
        public UsernameToken()
        {
            this.Type = TokenKeyType.USERNAMETOKEN;
        }

        public string UserName { get; set; }
        public string Token { get; set; }
    }
}
