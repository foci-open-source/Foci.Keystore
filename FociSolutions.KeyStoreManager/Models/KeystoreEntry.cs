﻿
namespace FociSolutions.KeyStoreManager.Models
{
    public class KeystoreEntry : IKeystoreEntry
    {
        public string Alias { get; set; }
        public TokenKeyType Type { get; set; }
        public MetaData MetaData { get; set; }
    }
}
