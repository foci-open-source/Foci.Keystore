﻿using FociSolutions.KeyStore.Services;
using FociSolutions.KeyStore.Services.Interface;
using FociSolutions.KeyStore.Models;
using System;
using Xunit;

namespace FociSolutions.KeyStoreManagerTests.Services
{
    public class KeystoreEncryptionServiceTests
    {
        #region Encrypt Tests
        [Fact]
        [Trait("Category", "EncryptSuccess")]
        public void EncryptTextSuccessfull()
        {
            IKeystoreEncryptionService keystoreEncryptionService = new KeystoreEncryptionService();
            var aKeyStore = new KeyCollection();
            var input = new {
                userToken = new UsernameToken()
                {
                    UserName = "username1",
                    Password = "password1",
                    Alias = "Alias1",
                    Token = "111111",
                    Type = TokenKeyType.TOKEN,
                    MetaData = new MetaData() { ExpiryTime = new DateTime() }
                }
            };

            aKeyStore.KeyStoreEntries.Add(input.userToken);
            var actual = keystoreEncryptionService.EncryptKeyStore(aKeyStore);
            Assert.NotNull(actual);



        }

        [Fact]
        [Trait("Category", "EncryptException")]
        public void EncryptNullTextException() {
            IKeystoreEncryptionService keystoreEncryptionService = new KeystoreEncryptionService();
            KeyCollection aKeyStore = null;
            Exception actual = Assert.Throws<ArgumentNullException>( () => keystoreEncryptionService.EncryptKeyStore(aKeyStore));
            Assert.NotNull(actual.Message);
        }
        #endregion

        #region Decrypt Tests
        [Fact]
        [Trait("Category", "DecryptSuccess")]
        public void DecryptTextSuccessfull() {
            IKeystoreEncryptionService keystoreEncryptionService = new KeystoreEncryptionService();
            var aKeyStore = new KeyCollection();
            var input = new
            {
                userToken = new UsernameToken()
                {
                    UserName = "username2",
                    Password = "password2",
                    Alias = "Alias3",
                    Token = "111111",
                    Type = TokenKeyType.TOKEN,
                    MetaData = new MetaData() { ExpiryTime = new DateTime() }
                }
            };
            aKeyStore.KeyStoreEntries.Add(input.userToken);
            var encryptedCollection = keystoreEncryptionService.EncryptKeyStore(aKeyStore);
            var actual = keystoreEncryptionService.DecryptKeyStore(encryptedCollection);
            Assert.NotNull(actual);
        }

        [Fact]
        [Trait("Category", "DecryptException")]
        public void DecryptNullTextException() {
            IKeystoreEncryptionService keystoreEncryptionService = new KeystoreEncryptionService();
            string encryptedCollection = null;
            Exception actual = Assert.Throws<ArgumentNullException>(() => keystoreEncryptionService.DecryptKeyStore(encryptedCollection));
            Assert.NotNull(actual);
        }
        #endregion
    }
}
