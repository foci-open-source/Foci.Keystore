﻿using FociSolutions.KeyStore.Cache;
using FociSolutions.KeyStore.Models;
using FociSolutions.KeyStore.Repositories.Interface;
using FociSolutions.KeyStore.Services;
using FociSolutions.KeyStore.Services.Interface;
using NSubstitute;
using System;
using System.Text;
using Xunit;

namespace FociSolutions.KeyStoreManager.Tests.Services
{
    public class KeyCollectionServiceTests
    {
        /// <summary>
        /// Asset that initialize keystore will success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void InitializeKeyStoreSuccessTest()
        {
            #region Substitutions

            var returnString = "someString";

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            keystoreEncryptionService.EncryptKeyStore(Arg.Any<KeyCollection>()).Returns(returnString);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var target = keyCollectionService;
            var actual = target.InitializeKeyCollection();

            Assert.True(actual);
        }

        #region UserName Tests

        /// <summary>
        /// Assert that insert user name token with attribuets to key collection will success.
        /// </summary>
        [Fact]
        [Trait("Category", "UserNameSuccess")]
        public void InsertUserNameTokenSuccessTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new UsernameToken()
            {
                UserName = "username1",
                Password = "password1",
                Alias = "AliasUserName0",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };

            var target = keyCollectionService;
            var actual = target.Insert(input);

            Assert.True(actual);
        }

        /// <summary>
        /// Assert that insert two user name tokens with same alias will throw argument exception.
        /// </summary>
        [Fact]
        [Trait("Category", "UserNameException")]
        public void InsertTwoUserNameTokensWithSameAliasTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();
            var entry = new UsernameToken()
            {
                UserName = "username1",
                Password = "password1",
                Alias = "AliasUserName0",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new UsernameToken()
            {
                UserName = "username1",
                Password = "password1",
                Alias = "AliasUserName0",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentException>(() => target.Insert(input));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that insert null user token will throw null reference exception.
        /// </summary>
        [Fact]
        [Trait("Category", "UserNameException")]
        public void InsertUserNameTokenNullTest()
        {
            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();


            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            UsernameToken input = null;
            var target = keyCollectionService;
            Exception actual = Assert.Throws<NullReferenceException>(() => target.Insert(input));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that retrieve user name will sucess with proper alias.
        /// </summary>
        [Fact]
        [Trait("Category", "UserNameSuccess")]
        public void RetrieveUserNameTokenTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();
            var entry = new UsernameToken()
            {
                Alias = "Alias2",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() }
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            UsernameToken input = new UsernameToken()
            {
                Alias = "Alias2",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() }
            };

            var target = keyCollectionService;
            var actual = target.Get<IKeystoreEntry>(input.Alias);
            Assert.Equal(input.Type, actual.Type);
        }

        /// <summary>
        /// Assert that retrieve user name token with null alias will throw argument null exception.
        /// </summary>
        [Fact]
        [Trait("Category", "UserNameException")]
        public void RetrieveUserNameTokenWithNullAliasTest()
        {
            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentNullException>(() => target.Get<IKeystoreEntry>(null));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that delete user name token with proper alias will success.
        /// </summary>
        [Fact]
        [Trait("Category", "UserNameSuccess")]
        public void DeleteUserNameTokenTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();
            UsernameToken entry = new UsernameToken()
            {
                Alias = "Alias4",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() }
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);

            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            UsernameToken input = new UsernameToken()
            {
                Alias = "Alias4",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() }
            };

            var target = keyCollectionService;
            var actual = target.Remove<IKeystoreEntry>(input.Alias);
            Assert.True(actual);
        }

        /// <summary>
        /// Assert that delete user name with null alias will throw argument null exception.
        /// </summary>
        [Fact]
        [Trait("Category", "UserNameException")]
        public void DeleteUserNameTokenNullAliasTest()
        {
            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();


            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentNullException>(() => target.Remove<IKeystoreEntry>(null));
            Assert.NotNull(actual.Message);
        }

        #endregion

        #region SimpleToken Tests

        /// <summary>
        /// Assert that insert simple token with attributes to key collection will success.
        /// </summary>
        [Fact]
        [Trait("Category", "SimpletokenSuccess")]
        public void InsertSimpleTokenSuccessTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new SimpleToken()
            {
                Alias = "AliasSimpleToken1",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() }
            };

            var target = keyCollectionService;
            var actual = target.Insert(input);

            Assert.True(actual);
        }

        /// <summary>
        /// Assert that insert two simple tokens with same alias will throw argument exception.
        /// </summary>
        [Fact]
        [Trait("Category", "SimpleTokenException")]
        public void InsertTwoSimpleTokensWithSameAliasTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();
            var entry = new SimpleToken()
            {

                Alias = "AliasSimpleToken2",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new SimpleToken()
            {
                Alias = "AliasSimpleToken2",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentException>(() => target.Insert(input));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that insert a null simple token will throw null reference exception.
        /// </summary>
        [Fact]
        [Trait("Category", "SimpleTokenException")]
        public void InsertSimpleTokenNullTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            SimpleToken input = null;
            var target = keyCollectionService;
            Exception actual = Assert.Throws<NullReferenceException>(() => target.Insert(input));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that retrieve simple token will success with proper alias.
        /// </summary>
        [Fact]
        [Trait("Category", "SimpleTokenSuccess")]
        public void RetrieveSimpleTokenTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();

            SimpleToken entry = new SimpleToken()
            {
                Alias = "AliasSimpleToken10",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() }
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            SimpleToken input = new SimpleToken()
            {
                Alias = "AliasSimpleToken10",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() }
            };

            var target = keyCollectionService;
            var actual = target.Get<IKeystoreEntry>(input.Alias);
            Assert.Equal(input.Type, actual.Type);
        }

        /// <summary>
        /// Assert that retrieve simple token with null alias will throw argument null exception.
        /// </summary>
        [Fact]
        [Trait("Category", "SimpleTokenException")]
        public void RetrieveSimpleTokenAliasNullTest()
        {
            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentNullException>(() => target.Get<IKeystoreEntry>(null));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that delete simple token with proper alias will success.
        /// </summary>
        [Fact]
        [Trait("Category", "SimpleTokenSuccess")]
        public void DeleteSimpleTokenWithPathDefinedTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();
            SimpleToken entry = new SimpleToken()
            {
                Alias = "AliasSimpleToken4",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() }
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            SimpleToken input = new SimpleToken()
            {
                Alias = "AliasSimpleToken4",
                Token = "111111",
                Type = TokenKeyType.TOKEN,
                MetaData = new MetaData() { ExpiryTime = new DateTime() }
            };

            var target = keyCollectionService;
            var actual = target.Remove<IKeystoreEntry>(input.Alias);
            Assert.True(actual);
        }

        /// <summary>
        /// Assert that delete simple token with null alias will throw argument null exception.
        /// </summary>
        [Fact]
        [Trait("Category", "SimpleTokenSuccess")]
        public void DeleteSimpleTokenWithNullAliasTest()
        {
            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();


            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentNullException>(() => target.Remove<IKeystoreEntry>(null));
            Assert.NotNull(actual.Message);
        }

        #endregion

        #region Keypair Tests

        /// <summary>
        /// Assert that insert key pair to key collection with attributes will success.
        /// </summary>
        [Fact]
        [Trait("Category", "KeyPairSuccess")]
        public void InsertKeyPairSuccessTestTest()
        {
            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new
            {
                keyPairToken = new KeyPair()
                {
                    Alias = "AliasKeyPair1",
                    Type = TokenKeyType.KEYPAIR,
                    PrivateKey = new PrivateKey() { KeyContent = "PrivateKey" },
                    PublicKey = new PublicKey() { KeyContent = "PublicKey" },
                    MetaData = new MetaData() { ExpiryTime = new DateTime() },
                }
            };

            var target = keyCollectionService;
            var actual = target.Insert(input.keyPairToken);

            Assert.True(actual);
        }

        /// <summary>
        /// Assert that insert two key pairs with same alias will throw argument exception.
        /// </summary>
        [Fact]
        [Trait("Category", "KeyPairException")]
        public void InsertTwoKeyPairsWithSameAliasTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();
            var entry = new KeyPair()
            {
                Alias = "AliasKeyPair3",
                Type = TokenKeyType.KEYPAIR,
                PrivateKey = new PrivateKey() { KeyContent = "PrivateKey" },
                PublicKey = new PublicKey() { KeyContent = "PublicKey" },
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };

            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new KeyPair()
            {
                Alias = "AliasKeyPair3",
                Type = TokenKeyType.KEYPAIR,
                PrivateKey = new PrivateKey() { KeyContent = "PrivateKey" },
                PublicKey = new PublicKey() { KeyContent = "PublicKey" },
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };
            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentException>(() => target.Insert(input));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that insert a null key pair will throw null reference exception.
        /// </summary>
        [Fact]
        [Trait("Category", "KeyPairException")]
        public void InsertKeyPairNullTest()
        {

            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();


            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            KeyPair input = null;
            var target = keyCollectionService;
            Exception actual = Assert.Throws<NullReferenceException>(() => target.Insert(input));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that retrieve key pair with proper alias will success.
        /// </summary>
        [Fact]
        [Trait("Category", "KeyPairSuccess")]
        public void RetrieveKeyPairSuccessTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();
            var entry = new KeyPair()
            {
                Alias = "AliasKeyPair4",
                Type = TokenKeyType.KEYPAIR,
                PrivateKey = new PrivateKey() { KeyContent = "PrivateKey" },
                PublicKey = new PublicKey() { KeyContent = "PublicKey" },
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new KeyPair()
            {
                Alias = "AliasKeyPair4",
                Type = TokenKeyType.KEYPAIR,
                PrivateKey = new PrivateKey() { KeyContent = "PrivateKey" },
                PublicKey = new PublicKey() { KeyContent = "PublicKey" },
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };

            var target = keyCollectionService;
            var actual = target.Get<IKeystoreEntry>(input.Alias);
            Assert.Equal(input.Type, actual.Type);
        }

        /// <summary>
        /// Assert that retrieve key pair with null alias will throw argument null exception.
        /// </summary>
        [Fact]
        [Trait("Category", "KeyPairException")]
        public void RetrieveKeyPairNullAliasTest()
        {
            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentNullException>(() => target.Get<IKeystoreEntry>(null));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that delete key pair with proper alias will success.
        /// </summary>
        [Fact]
        [Trait("Category", "KeyPairSuccess")]
        public void DeleteKeyPairSuccessTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();
            var entry = new KeyPair()
            {
                Alias = "AliasKeyPair6",
                Type = TokenKeyType.KEYPAIR,
                PrivateKey = new PrivateKey() { KeyContent = "PrivateKey" },
                PublicKey = new PublicKey() { KeyContent = "PublicKey" },
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new KeyPair()
            {
                Alias = "AliasKeyPair6",
                Type = TokenKeyType.KEYPAIR,
                PrivateKey = new PrivateKey() { KeyContent = "PrivateKey" },
                PublicKey = new PublicKey() { KeyContent = "PublicKey" },
                MetaData = new MetaData() { ExpiryTime = new DateTime() },
            };

            var target = keyCollectionService;
            var actual = target.Remove<IKeystoreEntry>(input.Alias);
            Assert.True(actual);
        }

        /// <summary>
        /// Assert that delete key pair with null alias will throw argument null exception.
        /// </summary>
        [Fact]
        [Trait("Category", "KeyPairException")]
        public void DeleteKeyPairNullAliasTest()
        {
            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentNullException>(() => target.Remove<IKeystoreEntry>(null));
            Assert.NotNull(actual.Message);
        }

        #endregion

        #region Certificate Tests

        /// <summary>
        /// Assert that insert certificate with attributes into Key Collection will success.
        /// </summary>
        [Fact]
        [Trait("Category", "CertificateSuccess")]
        public void InsertCertificateTokenSuccessTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new CertificateToken()
            {
                Alias = "cert1",
                RawData = Encoding.ASCII.GetBytes("certData2"),
                Type = TokenKeyType.CERTIFICATE
            };

            var target = keyCollectionService;
            var actual = target.Insert(input);

            Assert.True(actual);
        }

        /// <summary>
        /// Assert that insert certificate with same alise will throw Argument exception.
        /// </summary>
        [Fact]
        [Trait("Category", "CertificateException")]
        public void InsertCertificatesWithSameAliasTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();
            var entry = new CertificateToken()
            {
                Alias = "cert3",
                RawData = Encoding.ASCII.GetBytes("certData2"),
                Type = TokenKeyType.CERTIFICATE
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new CertificateToken()
            {
                Alias = "cert3",
                RawData = Encoding.ASCII.GetBytes("certData2"),
                Type = TokenKeyType.CERTIFICATE
            };

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentException>(
                () => target.Insert(input));
            string expected = string.Format(
                "Entry with alias: {0} already exists in keystore",
                input.Alias);
            Assert.Equal(expected, actual.Message);
        }

        /// <summary>
        /// Asset that insert null certificate will throw null reference enception.
        /// </summary>
        [Fact]
        [Trait("Category", "CertificateException")]
        public void InsertCertificateNullTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            CertificateToken input = null;
            var target = keyCollectionService;
            Exception actual = Assert.Throws<NullReferenceException>(() => target.Insert(input));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that retrieve will success with proper alise.
        /// </summary>
        [Fact]
        [Trait("Category", "CertificateSuccess")]
        public void RetrieveCertificateTest()
        {
            #region Substitutions
            
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();
            var entry = new CertificateToken()
            {
                Alias = "cert",
                RawData = Encoding.ASCII.GetBytes("certData"),
                Type = TokenKeyType.CERTIFICATE
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new CertificateToken()
            {
                Alias = "cert",
                RawData = Encoding.ASCII.GetBytes("certData"),
                Type = TokenKeyType.CERTIFICATE
            };

            var target = keyCollectionService;
            var actual = target.Get<IKeystoreEntry>(input.Alias);
            Assert.Equal(input.Type, actual.Type);
        }

        /// <summary>
        /// Assert that retrieve null alise will throw Arguement Null Exception.
        /// </summary>
        [Fact]
        [Trait("Category", "CertificateException")]
        public void RetrieveCertificateNullAliasTest()
        {
            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentNullException>(() => target.Get<IKeystoreEntry>(null));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Asset that delete certificate with alise will success.
        /// </summary>
        [Fact]
        [Trait("Category", "CertificateSuccess")]
        public void DeleteCertificateTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions
            
            var encryedData = "UnknownData";
            var anotherEncryedData = "AnotherUnknownData";
            var keyCollection = new KeyCollection();
            var entry = new CertificateToken()
            {
                Alias = "cert",
                RawData = Encoding.ASCII.GetBytes("certData"),
                Type = TokenKeyType.CERTIFICATE
            };
            keyCollection.KeyStoreEntries.Add(entry);

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(encryedData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyCollection);
            keystoreEncryptionService.EncryptKeyStore(keyCollection).Returns(anotherEncryedData);

            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var input = new CertificateToken()
            {
                Alias = "cert",
                RawData = Encoding.ASCII.GetBytes("certData"),
                Type = TokenKeyType.CERTIFICATE
            };

            var target = keyCollectionService;
            var actual = target.Remove<IKeystoreEntry>(input.Alias);
            Assert.True(actual);
        }

        /// <summary>
        /// Assert that delete with null alise will throw argument null exception.
        /// </summary>
        [Fact]
        [Trait("Category", "CertificateException")]
        public void DeleteCertificateAliasNullTest()
        {
            #region Substitutions

            var keystoreLocation = "my.keystore";
            var encryedData = "UnknownData";
            var keyCollection = new KeyCollection();


            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(keystoreLocation).Returns(encryedData);
            keystoreEncryptionService.DecryptKeyStore(encryedData).Returns(keyCollection);
            IKeyCollectionService keyCollectionService = new KeyCollectionService(
                keystoreEncryptionService, fileRepository);

            #endregion

            var target = keyCollectionService;
            Exception actual = Assert.Throws<ArgumentNullException>(() => target.Remove<IKeystoreEntry>(null));
            Assert.NotNull(actual.Message);
        }

        /// <summary>
        /// Assert that insert certificate file into certificate entry will success.
        /// </summary>
        [Fact]
        [Trait("Category", "CertificateSuccess")]
        public void InsertCertificateSuccessTest()
        {
            CollectionCache.ClearCache();

            #region Substitutions

            var keyStore = new KeyCollection();
            var certificateLocation = "./certfile.cer";
            var certData = "certData";
            var keyData = "keyData";
            var anotherKeyData = "anotherKeydata";
            var alisa = "cert1";

            var keystoreEncryptionService = Substitute.For<IKeystoreEncryptionService>();
            var fileRepository = Substitute.For<IFileRepository>();

            fileRepository.Read(Arg.Any<string>()).Returns(certData);
            fileRepository.Read(Arg.Any<string>()).Returns(keyData);
            fileRepository.Write(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            keystoreEncryptionService.DecryptKeyStore(Arg.Any<string>()).Returns(keyStore);
            keystoreEncryptionService.EncryptKeyStore(keyStore).Returns(anotherKeyData);
            IKeyCollectionService keycollectionService = new KeyCollectionService(keystoreEncryptionService, fileRepository);

            #endregion

            var input = new
            {
                certificateFilePath = certificateLocation,
                entry = new CertificateToken()
                {
                    Alias = alisa,
                    Type = TokenKeyType.CERTIFICATE
                }
            };

            var target = keycollectionService;

            var actual = target.InsertCertificate(input.certificateFilePath, input.entry);

            Assert.True(actual);
        }

        #endregion

    }
}
