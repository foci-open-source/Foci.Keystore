# How to use the Keystore

The [Keystore](https://www.nuget.org/packages/FociSolutions.KeyStore/) is a secure vault that can be used as a standalone in any operating system and save all your credentials, from tokens to certificates. With [Orbital Bus](http://orbitalbus.com/), it is used to store and retrieve credentials to make ssl connections with RabbitMQ and Consul. This is a how-to guide to show you how to use the Keystore in code and using the **KeystoreManager**.

The structure of a Keystore entry is:

* **Alias:** This is the common field in a Keystore Entry. It **is required** to be **unique** as all the credentials are in one same keystore.
* **Entry Data:** This is sensitive information of the credintial that's encrypted and stored in the Keystore.

The following are the type of credentials you can save to the Keystore:

* **Certificate:**  This credential entry accomidates a certificate file. The user will input the path where the certificate like a X.509 certificates is located.

* **Keypair:** This credential entry accomodates a public key and private key.

* **Username Token:** This credential entry is for a username/password credential type. A token field can be filled but is optional. 

* **Token:** This credential entry is for tokens.

## Keystore in-Code Use

The Keystore is avaialbe as a [Nuget Package](https://www.nuget.org/packages/FociSolutions.KeyStore/), where it could be referenced in any project and utilized.  The Keystore is desinged to allow quick intergration into any system. The integration of the Keystore can be as simple as these 3 quick steps:

1. Create a new project in Visual Studio project. 

2. Download the [Keystore nuget package](https://www.nuget.org/packages/FociSolutions.KeyStore/) for the project.

3. Now copy this simple code in your Main():

```csharp

			Keystore.Initialize();

            while (true)
            {
                Console.WriteLine("\n I'll introduce an username to the Keystore. First, give me an alias! : ");
                var alias = Console.ReadLine();

                Console.WriteLine("\n How about a token? : ");
                var token = Console.ReadLine();

                Console.WriteLine("\n Give a username! : ");
                var username = Console.ReadLine();

                Console.WriteLine("\n And a password! : ");
                var password = Console.ReadLine();


                var SimpleToken = new UsernameToken()
                    {
                        Alias = alias,
                        Token = token,
                        UserName = username,
                        Password = password
                    };

                 Keystore.Insert(SimpleToken);

                Console.WriteLine("\n Now I'll show you it worked! \n Tell me the alias of your credentials: ");
                var aliasToLook = Console.ReadLine();

                var credentials = Keystore.Get<UsernameToken>(aliasToLook);

                Console.WriteLine("\n This are your credentials: \n Alias: {0} \n Token: {1} \n Username: {2} \n Password: {3} ", credentials.Alias, credentials.Token, credentials.UserName, credentials.Password);

                       
                    
            }

```
Build and run the program. You will be able to insert all the username/password credentials you want. **Remember!** You can't repeat the alias.

Your console application output should look something like this:

```
I'll introduce an username to the Keystore. First, give me an alias! : 
credential_demo

How about a token? :
token_demo

Give a username! :
guest

And a password! :
guest

Now I'll show you it worked! 

Tell me the alias of your credentials:
credential_demo

This are your credentials: 
Alias: credential_demo
Token: token_demo
Username: guest 
Password: guest

```

If you are looking for a practical example for in-code use of the Keystore, then you can visit [Orbital Bus Api](http://orbitalbus.com/api/index.html) to review the `KeystoreService` and `KeystoreRepository` classes.

## Using the KeystoreManager!

This other option allows you to initilize and perform CRUD operations on the Keystore directly. The open source solution comes also with a CLI tool to manage the Keystore. This tool is the called **Keystore Manager**. The Keystore Manager manages your Keystore; it enables you to add, edit, and remove credentials. The following is a short demonstration of how it works by adding a username/password credentials.

1. Download the [Keystore release](https://gitlab.com/foci-open-source/Foci.Keystore/tags/v0.1.0). 

2. Open the solution and run the `KeystoreManager` project.

3. First you need to initialize the keystore:

```
InitializeKeyStore

Executing InitializeKeyStore (Creates an empty encrypted Keystore):
```

4. Let's insert a Username Token:

```
InsertUsernameToken -a demo_alias -u demo_username -p demo_password -t demo_token

Executing InsertUsernameToken (Inserts the given username and token to the provided Keystore):
    Alias : demo_alias
    Password : demo_password
    Token : demo_token
    UserName : demo_username
```

5. Let's see if the credentials were saved:
GetUsernameToken -a demo_alias

Executing GetUsernameToken (Gets username token with alias provided):
    Alias : demo_alias

FociSolutions.KeyStore.Models.UsernameToken

6. Now that we confirmed the Keystore saved our credentials. Let's delete them!

```
RemoveKey -a demo_alias

Executing RemoveKey (Remove tkey by passing the alias to a provided Keystore):
    Alias : demo_alias
```

