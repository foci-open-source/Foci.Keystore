# Welcome to the Foci Solutions .NET Keystore!

This project is intended to provide a repository for secure storage of key pairs, certificates, and tokens. Using the Keystore, you can store sensitive credentials against the domain user login and rely on a single point of security.

For more information check out our [README](articles/README.html). Also be sure to check out our [open-source repository](?).

**Check out our Beta release here: [https://gitlab.com/foci-open-source/Foci.Keystore/tags/v0.1.0](https://gitlab.com/foci-open-source/Foci.Keystore/tags/v0.1.0).**