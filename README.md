# Foci Solutions .NET KeyStore

This project is intended to provide a repository to secure the storage of private-public key pairs, authorization or public key certificates, and Authorization tokens. The Keystore is connected to your user account and stores values against an alias. Using the Keystore, you can store sensitive credentials against the domain user login and rely on a single point of security.  

The solution consists of three VisualStudio projects.  They are as follows:
* FociSolutions.KeyStore - Contains Keystore model definitions, file I/O classes, and services for CRUD to key entries and certificates insertion.
* FociSolutions.KeyStoreManager - The KeyStore Manager project provides a command line interface to users to do CRUD operations to the Keystore and contained certificates.
* FociSolutions.KeyStore.KeyStoreManager.Tests - A unit tests project to cover the test cases of Keystore operations.


## Licensing Information

The Keystore is licensed under the 4-Clause BSD license for open-source software. Please see the [LICENSE](LICENSE.md) file for more information.  

## Contact Information
For inquiries about the project, please e-mail us at [opensource@focisolutions.com](mailto:opensource@focisolutions.com).